@everywhere function pick_config(N)
  return round(Int, rand([-1, 1], N, N))
end


@everywhere function config_energy(lattice)
  fp = dot(vec(lattice[1:end-1, :]), vec(lattice[2:end, :]))
  sp = dot(vec(lattice[:, 1:end-1]), vec(lattice[:, 2:end]))
  return (-fp - sp)
end

@everywhere function mmc(beta, n, n_samples)
  config = pick_config(n)
  energy = config_energy(config)
  energies = zeros(0)
  mags = zeros(0)
  n_wait = round(Int, n_samples / 10)
  for l in 1:(n_samples + n_wait)
      for z in 1:(n*n)
          # pick a random atom
          i = rand(1:n)
          j = rand(1:n)
          # flip its spin
          config[i, j] = -config[i, j]
          # Determine difference in energy
          energy_prime = config_energy(config) - energy
          q = exp(-beta * energy_prime)
          r = rand()
          # Always do it if the energy is higher, else accept it with a
          # probability
          if q > r
              energy += energy_prime
          else
              # if it is not accepted, revert to old situation
              config[i, j] = -config[i, j]
          end
      end
      # if the system is somewhat relaxed, store the energy
      if l > n_wait
          push!(energies, energy)
          push!(mags, sum(vec(config)))
      end
  end
  return energies, mags
end

@everywhere function plot_parallel(tup)
    # (beta, n, nsamp)
    beta = tup[1]
    n = tup[2]
    nsamp = tup[3]
    Energies, Mags = mmc(beta, n, nsamp)
    energy_per_spin = mean(Energies) / (n * n)
    spec_heat_per_spin = (var(Energies) * beta ** 2) / (n * n)
    average_magnetization = mean(Mags) / (n * n)
    return (1 / beta, energy_per_spin, spec_heat_per_spin, average_magnetization)
end

# pool = multiprocessing.Pool()
tic()
num_samples = 1000
num_points = 200
n = 50
betas = map(x -> 1 / x, linspace(0.2, 4, num_points))
zipped = pmap(plot_parallel, zip(betas, repmat([n], num_points), repmat([num_samples], num_points)))
ts = []
energy_per_spin = []
spec_heat_per_spin = []
average_magnetization = []
for tup in zipped
  push!(ts, tup[1])
  push!(energy_per_spin, tup[2])
  push!(spec_heat_per_spin, tup[3])
  push!(average_magnetization, tup[4])
end
println(ts)
println(energy_per_spin)
toc()
return 0