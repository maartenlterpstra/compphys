# return a line of spins
function pick_configuration(size)
	lattice = round(Int, rand(size)) * 2 - 1
end

function print_help()
	println("Usage: mmc.jl SIZE STEPS")
	println("SIZE: number of spins")
	println("STEPS: number of Metropolis Monte-Carlo steps to perform")
end

function find_energy_change(lattice, random_idx)
	curr_energy = 0
	new_energy = 0
	# boundary conditions, do not wrap around
	if random_idx == 1
		curr_energy = lattice[random_idx] * lattice[random_idx + 1]
		new_energy = -lattice[random_idx] * lattice[random_idx + 1]
	elseif random_idx == length(lattice)
		curr_energy = lattice[random_idx] * lattice[random_idx - 1]
		new_energy = -lattice[random_idx] * lattice[random_idx - 1]
	else
		curr_energy = lattice[random_idx - 1] * lattice[random_idx] +  lattice[random_idx] * lattice[random_idx + 1]
		new_energy = lattice[random_idx - 1] * -lattice[random_idx] - lattice[random_idx] * lattice[random_idx + 1]
	end
	return curr_energy - new_energy
end

function find_energy(lattice, size)
	return dot(lattice[1:end-1], lattice[2:end])
end

function mmc(beta, size, steps, wait)
	lattice = pick_configuration(size)
	e = lattice_energy(lattice)
	energies = []
	for i in 0:(steps + wait)
		random_idx = rand(1:size)
		lattice_flip = lattice
		lattice_flip[random_idx] = -lattice_flip[random_idx]
		delta_e = lattice_energy(lattice_flip) - e
		if exp(-beta * delta_e) > rand()
			lattice = lattice_flip
			e += delta_e
		end
		if i > wait
			# record energy change
			push!(energies, e)
		end
	end
	return energies
end

if length(ARGS) != 2
	print_help()
else

	Ts = 0.2:0.2:4
	sz = parse(Int, ARGS[1])
	steps = parse(Int, ARGS[2])
	num_wait_steps = round(Int, steps / 10)
	for t in Ts
		beta = 1.0 / t
		# println(string(beta, " ", sz, " ", steps, " ", num_wait_steps))
		energy, energy2 = mmc(1 / t, sz, steps, num_wait_steps)
		println(string(energy, ", ", -tanh(beta)))
	end
end