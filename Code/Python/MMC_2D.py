#!/usr/bin/python
import numpy as np
import random
import multiprocessing
from math import log, sinh
from itertools import repeat
import sys
import time
import matplotlib.pyplot as plt


def pick_config(N):
    options = [-1, 1]
    return np.random.choice(options, (N, N))


def config_energy(lattice):
    # efficient energy calculation using dot products
    fp = np.dot(lattice[:-1, :].ravel(), lattice[1:, :].ravel())
    sp = np.dot(lattice[:, :-1].ravel(), lattice[:, 1:].ravel())
    return -fp - sp

config = None


def find_change_in_energy(i, j, config):
    m, n = config.shape
    orig, new = 0.0, 0.0
    if i == 0:
        if j == 0:
            # top left corner
            orig = -config[i, j] * config[i + 1, j] - \
                config[i, j] * config[i, j + 1]
            config[i, j] = -config[i, j]
            new = -config[i, j] * config[i + 1, j] - \
                config[i, j] * config[i, j + 1]
            config[i, j] = -config[i, j]
        elif j == n - 1:
            # top right corner
            orig = -config[i, j] * config[i + 1, j] - \
                config[i, j - 1] * config[i, j]

            config[i, j] = -config[i, j]
            new = -config[i, j] * config[i + 1, j] - \
                config[i, j - 1] * config[i, j]
            config[i, j] = -config[i, j]

        else:
            # just along top strip
            left = config[i, j - 1] * config[i, j]
            right = config[i, j] * config[i, j + 1]
            orig = -config[i, j] * config[i + 1, j] - left - right
            config[i, j] = -config[i, j]
            left = config[i, j - 1] * config[i, j]
            right = config[i, j] * config[i, j + 1]
            new = -config[i, j] * config[i + 1, j] - left - right
            config[i, j] = -config[i, j]
    elif i == n - 1:
        if j == 0:
            # bottom left corner
            orig = -config[i - 1, j] * config[i, j] - \
                config[i, j] * config[i, j + 1]
            config[i, j] = -config[i, j]
            new = -config[i - 1, j] * config[i, j] - \
                config[i, j] * config[i, j + 1]
            config[i, j] = -config[i, j]

        elif j == n - 1:
            # bottom right corner
            orig = -config[i - 1, j] * config[i, j] - \
                config[i, j - 1] * config[i, j]
            config[i, j] = -config[i, j]
            new = -config[i - 1, j] * config[i, j] - \
                config[i, j - 1] * config[i, j]
            config[i, j] = -config[i, j]
        else:
            # along bottom strip
            left = config[i, j - 1] * config[i, j]
            right = config[i, j] * config[i, j + 1]
            orig = -config[i, j] * config[i - 1, j] - left - right
            config[i, j] = -config[i, j]
            left = config[i, j - 1] * config[i, j]
            right = config[i, j] * config[i, j + 1]
            new = -config[i, j] * config[i - 1, j] - left - right
            config[i, j] = -config[i, j]

    elif j == 0:
        # along left strip, rest handled
        above = config[i - 1, j] * config[i, j]
        below = config[i, j] * config[i + 1, j]
        orig = -config[i, j] * config[i, j + 1] - above - below
        config[i, j] = -config[i, j]
        above = config[i - 1, j] * config[i, j]
        below = config[i, j] * config[i + 1, j]
        new = -config[i, j] * config[i, j + 1] - above - below
        config[i, j] = -config[i, j]

    elif j == n-1:
        # along right strip, rest handled
        above = config[i - 1, j] * config[i, j]
        below = config[i, j] * config[i + 1, j]
        orig = -config[i, j] * config[i, j - 1] - above - below
        config[i, j] = -config[i, j]
        above = config[i - 1, j] * config[i, j]
        below = config[i, j] * config[i + 1, j]
        new = -config[i, j] * config[i, j - 1] - above - below
        config[i, j] = -config[i, j]

    else:
        # all directions available
        above = config[i - 1, j] * config[i, j]
        below = config[i, j] * config[i + 1, j]
        left = config[i, j - 1] * config[i, j]
        right = config[i, j] * config[i, j + 1]
        orig = -left - right - above - below
        config[i, j] = -config[i, j]
        above = config[i - 1, j] * config[i, j]
        below = config[i, j] * config[i + 1, j]
        left = config[i, j - 1] * config[i, j]
        right = config[i, j] * config[i, j + 1]
        new = -left - right - above - below
        config[i, j] = -config[i, j]

    return new - orig


def mmc(beta, n, n_samples, config=None, n_wait=None):
    # Find the optimal configuration for a temperature using MMC
    # start with a random configuration and determine its energy
    if config is None:
        config = pick_config(n)
    energy = config_energy(config)
    energies = np.array([])
    mags = np.array([])
    # default waiting time is N / 10
    if n_wait is None:
        n_wait = n_samples // 10
    # Take a number of samples + waiting time (to let it relax)
    for l in range(n_samples + n_wait):
        for z in range(n*n):
            # pick a random atom
            j = np.random.randint(0, n)
            i = np.random.randint(0, n)
            # Determine difference in energy
            energy_prime = find_change_in_energy(i, j, config)
            q = np.exp(-beta * energy_prime)
            r = random.random()
            # Always accept if it's better, else accept it with a probability
            if q > r:
                energy += energy_prime
                config[i, j] = -config[i, j]
            # if it is not accepted, revert to old situation
        # if the system is somewhat relaxed, store the energy
        if l > n_wait:
            energies = np.append(energies, energy)
            mags = np.append(mags, np.sum(config.ravel()))
    return (energies, mags, config)


def compute_lattice(beta_n_nsamp_config):
    # I do not like PEP-3113
    beta, n, nsamp, config = beta_n_nsamp_config
    Energies, Mags, config = mmc(beta, n, nsamp, config)
    energy_per_spin = np.mean(Energies) / (n * n)
    spec_heat_per_spin = (np.var(Energies) * beta ** 2) / (n * n)
    avg_magnetization = np.mean(Mags) / (n * n)
    return (1 / beta, energy_per_spin, spec_heat_per_spin, avg_magnetization, config)


if len(sys.argv) != 3:
    print("Usage: mmc_2d <num points> <num samples>")
    print("     num points:  number of temperatures between 0.2 and 4")
    print("                  Note: by taking this value as 20 you will get")
    print("                        the interval the assignment requires")
    print("     num samples: number MMC steps to perform per temperature")
    sys.exit(-1)


def calc(result_queue, betas, n, num_samples, config=None):
    ts = []
    energies_ps = []
    spec_heats_ps = []
    mags_ps = []

    for i, beta in enumerate(betas[::-1]):
        tic = time.time()
        t, energy_ps, spec_heat_ps, mag_ps, config = compute_lattice(
            (beta, n, num_samples, config))
        elapsed = time.time() - tic
        print("Done {0}, {1}% done. This took {2} seconds".format(
            beta, (((i + 1) / float(len(betas))) * 100.0), elapsed))
        ts.append(t)
        energies_ps.append(energy_ps)
        spec_heats_ps.append(spec_heat_ps)
        mags_ps.append(mag_ps)

    result_queue.put((ts, energies_ps, spec_heats_ps, mags_ps))

num_points = int(sys.argv[1])
num_samples = int(sys.argv[2])
betas = [1/t for t in np.linspace(0.2, 4, num_points)]
# # Compute all statistics for 10, 50, and 100 atoms respectively in parallel
# # but sequentially.
result_queue = multiprocessing.Queue()
jobs = [multiprocessing.Process(
    target=calc, args=(result_queue, betas, n, num_samples)) for n in [10,
                                                                       50, 100]]

for job in jobs:
    job.start()
for job in jobs:
    job.join()
results = [result_queue.get() for j in jobs]

ts_10, energies_ps_10, spec_heats_ps_10, mags_ps_10 = results[0]
ts_50, energies_ps_50, spec_heats_ps_50, mags_ps_50 = results[1]
ts_100, energies_ps_100, spec_heats_ps_100, mags_ps_100 = results[2]

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
crit_temp = 2 / (log(1 + (2 ** 0.5)))

theo_mag = [(1 - sinh(2 / t) ** -4) ** (1/8) if t <
            crit_temp else 0 for t in ts_10]

# Scatter plot of the energy
e_10 = plt.scatter(ts_10, energies_ps_10, color='blue')
e_50 = plt.scatter(ts_50, energies_ps_50, color='green')
e_100 = plt.scatter(
    ts_100, energies_ps_100, color='magenta')
axes = plt.gca()
energy_title = "Average enery per spin as a function of the " + \
    "temperature - {0} samples".format(num_samples)
plt.title(energy_title)
axes.set_xlabel("Temperature")
axes.set_ylabel("$E$")

plt.legend((e_10, e_50, e_100),
           ('10 atoms', '50 atoms', '100 atoms'),
           scatterpoints=1,
           loc='upper left',
           ncol=1,
           fontsize=12)

energy_filename = 'energies-{0}-{1}.eps'.format(num_samples, num_points)
# plt.savefig(energy_filename, format='eps', dpi=1000)

# Scatter plot of the specific heat
plt.figure()
c_10 = plt.scatter(ts_10, spec_heats_ps_10, color='blue')
c_50 = plt.scatter(
    ts_50, spec_heats_ps_50, color='green')
c_100 = plt.scatter(
    ts_100, spec_heats_ps_100, color='magenta')
plt.legend((c_10, c_50, c_100),
           ('10 atoms', '50 atoms', '100 atoms'),
           scatterpoints=1,
           loc='upper left',
           ncol=1,
           fontsize=12)

axes = plt.gca()
heat_title = "Specific heat per spin as a function of the " + \
    "temperature - {} samples".format(num_samples)
plt.title(heat_title)
axes.set_xlabel("Temperature")
axes.set_ylabel("$C_T$")
axes.set_ylim([0, 3])
heats_filename = 'heats-{0}-{1}.eps'.format(num_samples, num_points)
# plt.savefig(heats_filename, format='eps', dpi=1000)

# Scatter plot of the magnetiztion per spin.
# Note it plots the absolute value
plt.figure()
m101000 = plt.scatter(
    ts_10, np.abs(mags_ps_10), color='blue')
m501000 = plt.scatter(
    ts_50, np.abs(mags_ps_50), color='green')
m1001000 = plt.scatter(
    ts_100, np.abs(mags_ps_100), color='magenta')
mtheo = plt.scatter(ts_10, theo_mag, color='red')
plt.legend((m101000, m501000, m1001000, mtheo),
           ('10 atoms', '50 atoms', '100 atoms', 'Theoretical'),
           scatterpoints=1,
           loc='upper right',
           ncol=2,
           fontsize=12)

axes = plt.gca()
mag_title = "Average magnetization per spin as a function of the " + \
    "temperature - {0} samples".format(num_samples)
plt.title(mag_title)

axes.set_xlabel("Temperature")
axes.set_ylabel("$M$")
mags_filename = 'magnetizations-{0}-{1}.eps'.format(
    num_samples, num_points)
# plt.savefig(mags_filename, format='eps', dpi=1000)
plt.show()
