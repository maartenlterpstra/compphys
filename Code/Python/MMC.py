#!/usr/bin/env python
import numpy as np
from tabulate import tabulate
import random


def config_energy(S):
    # efficient energy calculation using dot products
    return -np.dot(S[:-1], S[1:])


def pick_config(N):
    options = [-1, 1]
    return np.random.choice(options, N)


def determine_average_energy(beta, energy, n):
    avg_energy = np.mean(energy) / n
    theo_energy = -np.tanh(beta)
    acc = 1 - np.abs((np.abs(avg_energy - theo_energy) / avg_energy))
    return (avg_energy, theo_energy, acc)


def determine_specific_heat(beta, energy, n):
    spec_heat = (np.var(energy) / n) * (beta ** 2)
    theo_heat = (beta / np.cosh(beta)) ** 2
    acc = 1 - np.abs((np.abs(spec_heat - theo_heat) / spec_heat))
    return (spec_heat, theo_heat, acc)


def find_energy_change(random_idx, config):
    conf_len = len(config)
    fp = 0
    sp = 0
    # no wrap around, so edge cases handled separately
    if random_idx == 0:
        fp = config[0] * config[1] - (-config[0] * config[1])
    elif random_idx == conf_len - 1:
        sp = config[-2] * config[-1] - (-config[-1] * config[-2])
    else:
        fp = config[random_idx] * config[random_idx + 1] - \
            (-config[random_idx] * config[random_idx + 1])
        sp = config[random_idx-1] * config[random_idx] - \
            (-config[random_idx] * config[random_idx - 1])
    return fp + sp


def mmc(beta, n, n_samples, config=None, n_wait=None):
    # start with a random configuration and determine its energy
    if config is None:
        config = pick_config(n)
    energy = config_energy(config)
    energies = np.array([])
    # default waiting time is N / 10
    if n_wait is None:
        n_wait = int(n_samples / 10)
    # Take a number of samples + waiting time (to let it relax)
    for l in range(n_samples + n_wait):
        for z in range(n):
            # pick a random atom
            j = np.random.randint(0, n)
            # Determine difference in energy
            energy_prime = find_energy_change(j, config)
            q = np.exp(-beta * energy_prime)
            r = random.random()
            # Always do it if the energy is lower, else accept it with a
            # probability
            if energy_prime < 0 or q > r:
                # flip its spin
                config[j] = -config[j]
                energy += energy_prime
        # if the system is somewhat relaxed, store the energy
        if l > n_wait:
            energies = np.append(energies, energy)
    return (energies, config)


def do_simulation(n, sample):
    betas = [1/t for t in np.linspace(0.2, 4, 20)]
    headers = ["T", "beta", "U_MC", "C_MC", "U_theory", "C_theory", "Acc"]
    # here the results are stored. Key is the tuple of number of samples
    # and the number of atoms. Value is an array with statistics in the order
    # of the headers array
    datas = dict()

    # Last column separately added so tabulate doesn't mess up the
    # formatting
    data = np.zeros((len(betas), len(headers) - 1))
    a = np.array([""] * len(betas))
    data = np.c_[data, a]
    config = None
    # reverse betas for aesthetics
    for i, beta in enumerate(betas[::-1]):
        (energy, config) = mmc(beta, n, sample, config)

        (avg_energy, theo_e, acc_e) = determine_average_energy(
            beta, energy, n)
        (spec_heat, theo_c, acc_h) = determine_specific_heat(
            beta, energy, n)
        table_row = [1 / beta, beta, avg_energy, spec_heat,
                     theo_e, theo_c, "{:.2%}".format((acc_h + acc_e) / 2)]
        data[i, :] = table_row

    datas[(sample, n)] = data
    print("Done {0} atoms at {1} samples".format(n, sample))
    for (s, n) in datas.keys():
        print("N = {1}, {0} samples".format(s, n))
        print(
            tabulate(datas[(s, n)], headers=headers, floatfmt='.2E'))
        print("")

for n in [10, 100, 1000]:
    for sample in [1000, 10000]:
        do_simulation(n, sample)
