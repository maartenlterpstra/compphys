function [U, C, M] = MMC2D(T, N, Nwait, Nsamples)
    % Implementation of the Metropolis MonteCarlo algorithm
    % Pick a configuration
    spins = (randi([1, 2], N) - 1) * 2 - 1;
    E = Energy2D_slow(spins);
    minusBeta = -1/T;
    Energies = zeros(1, Nsamples);
    Mags = zeros(1, Nsamples);
    for t=1:Nsamples + Nwait
        i = round(rand() *(N-1) + 1);
        j = round(rand() *(N-1) + 1);
        spins(i, j) = -spins(i, j);
        dE = Energy2D_slow(spins) - E;
        if dE < 0 || minusBeta * dE > log(rand())
            E = E + dE;
        else
            spins(i, j) = -spins(i, j);
        end
        if t > Nwait
            Energies(t - Nwait) = E;
            Mags(t - Nwait) = sum(spins(:));
        end
    end
    assert(size(Energies, 2) == Nsamples)
    U = (mean(Energies)) / (N ^ 2);
    C = var(Energies) / (N ^ 2 * T^2);
    M = sum(Mags) / (N ^ 2);
end

