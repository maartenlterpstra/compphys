function [ Energy ] = Energy2D_slow( configuration )
%ENERGY2D_SLOW Summary of this function goes here
%   Detailed explanation goes here
    [N, M] = size(configuration);
    first_part = 0;
    second_part = 0;
    
    for i = 1:N-1
        for j=1:M
            first_part = first_part + configuration(i, j) * configuration(i + 1, j);
        end
    end
    for i = 1:N
        for j=1:M-1
            second_part = second_part + configuration(i, j) * configuration(i, j + 1);
        end
    end
    Energy = -first_part - second_part;
end

