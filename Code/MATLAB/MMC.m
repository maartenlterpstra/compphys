function [U, C] = MMC(T, N, Nwait, Nsamples)
    % Implementation of the Metropolis MonteCarlo algorithm
    % Pick a configuration
    spins = (randi(2, 1, N) - 1) * 2 - 1;
    E = Energy(spins);
    minusBeta = -1/T;
    Energies = zeros(1, Nsamples);
    
    for i=1:Nsamples + Nwait
        j = round(rand() *(N-1) + 1);
        spins(j) = -spins(j);
        dE = Energy(spins) - E;
        if dE < 0 || minusBeta * dE > log(rand())
            E = E + dE;
        else
            spins(j) = -spins(j);
        end
        if i > Nwait
            Energies(i - Nwait) = E;
        end
    end
    assert(size(Energies, 2) == Nsamples)
    U = (mean(Energies)) / N;
    C = var(Energies) / (N * T^2);
end

