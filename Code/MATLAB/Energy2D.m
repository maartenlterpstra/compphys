function [ Energy ] = Energy2D( configuration )
%ENERGY2D Summary of this function goes here
%   Detailed explanation goes here
    [N, M] = size(configuration);
    assert(N == M);
    first_part = configuration(1:N-1, :) .* configuration(2:N, :);
    second_part = configuration(:, 1:M-1) .* configuration(:, 2:M);
    Energy = -sum(first_part(:)) - sum(second_part(:));
end

