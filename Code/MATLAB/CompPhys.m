%% 1D
Ts_1D = 4:-0.2:0.2;
Ns_1D = [10, 100, 1000];
Nsamples_1D = [1000, 10000];
headers_1D = {'T', 'Beta', 'U_MC', 'C_MC', 'U_T', 'C_T', 'Acc_U', 'Acc_C'};
data_1D = zeros(numel(Ts_1D), numel(headers_1D), numel(Ns_1D));
tCnt = 1;
for t=Ts_1D
    beta = 1 / t;
    nNum = 1;
    for n=Ns_1D
        nsamp = 10000;
        [U, C] = MMC(t, n, round(nsamp / 10), nsamp);
        theoU = -tanh(beta);
        theoC = (beta / cosh(beta))^2;
        % make sure accuracy is in [0, 100]
        accU = U / theoU;
        if accU > 1 || accU < 0
            accU = theoU / U;
        end
        accC = C / theoC;
        if accC > 1 || accC < 0
            accC = theoC / C;      
        end
        data_1D(tCnt, :, nNum) = [t, beta, U, C, theoU, theoC, accU, accC]; 
        nNum = nNum + 1;
    end
    tCnt = tCnt + 1;
end

displaytable(data_1D(:, :, 1), headers_1D)
displaytable(data_1D(:, :, 2), headers_1D)
displaytable(data_1D(:, :, 3), headers_1D)

%% 2D
Ts_2D = 4:-0.2:0.2;
Ns_2D = [10, 50, 100];
Nsamples_2D = [1000, 10000];
headers_2D = {'T', 'Beta', 'U_MC', 'C_MC', 'M_MC', 'U_T', 'C_T', 'M_T', 'Acc_U', 'Acc_C', 'Acc_M'};
data_2D = zeros(numel(Ts_2D), numel(headers_2D), numel(Ns_2D));
tCnt = 1;
critical_temp = 2 / log(1 + sqrt(2));
for t=Ts_2D
    beta = 1 / t;
    nNum = 1;
    for n=Ns_2D
        nsamp = 10000;
        [U, C, M] = MMC2D(t, n, round(nsamp / 10), nsamp);
        theoU = -tanh(beta);
        theoC = (beta / cosh(beta))^2;
        theoM = 0;
        if t < critical_temp
            theoM = (1 - sinh(2*(1/t))^(-4)) ^ (1/8);
        end
        % make sure accuracy is in [0, 100]
        accU = U / theoU;
        if accU > 1 || accU < 0
            accU = theoU / U;
        end
        accC = C / theoC;
        if accC > 1 || accC < 0
            accC = theoC / C;      
        end
        accM = M / theoM;
        if accM > 1 || accM < 0
            accM = accM / M;      
        end
        data_2D(tCnt, :, nNum) = [t, beta, U, C, M, theoU, theoC, theoM, accU, accC, accM]; 
        nNum = nNum + 1;
    end
    tCnt = tCnt + 1;
end
Ns_2D(1)
displaytable(data_2D(:, :, 1), headers_2D)
Ns_2D(2)
displaytable(data_2D(:, :, 2), headers_2D)
Ns_2D(3)
displaytable(data_2D(:, :, 3), headers_2D)
