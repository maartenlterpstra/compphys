%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thin Sectioned Essay
% LaTeX Template
% Version 1.0 (3/8/13)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original Author:
% Nicolas Diaz (nsdiaz@uc.cl) with extensive modifications by:
% Vel (vel@latextemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a4paper, 11pt]{article} % Font size (can be 10pt, 11pt or 12pt) and paper size (remove a4paper for US letter paper)

% \usepackage[protrusion=true,expansion=true]{microtype} % Better typography
\usepackage{graphicx} % Required for including pictures
\usepackage{wrapfig} % Allows in-line images
\usepackage{epstopdf}
\usepackage{url}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[T1]{fontenc} % Required for accented characters
% \usepackage{mathpazo} % Use the Palatino font
% \usepackage{eulervm}
\usepackage{algorithm}
\usepackage[a4paper]{geometry}
\usepackage{todonotes}
\usepackage{algpseudocode} 
\usepackage[toc,page]{appendix}
\usepackage{listings}
\usepackage{minted}
\usepackage{newclude}
\usepackage[hidelinks]{hyperref}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[createtips]{fancytooltips}

\makeatletter

% define a macro \Autoref to allow multiple references to be passed to \autoref
\newcommand\Autoref[1]{\@first@ref#1,@}
\def\@throw@dot#1.#2@{#1}% discard everything after the dot
\def\@set@refname#1{%    % set \@refname to autoefname+s using \getrefbykeydefault
    \edef\@tmp{\getrefbykeydefault{#1}{anchor}{}}%
    \def\@refname{\@nameuse{\expandafter\@throw@dot\@tmp.@autorefname}s}%
}
\def\@first@ref#1,#2{%
  \ifx#2@\autoref{#1}\let\@nextref\@gobble% only one ref, revert to normal \autoref
  \else%
    \@set@refname{#1}%  set \@refname to autoref name
    \@refname~\ref{#1}% add autoefname and first reference
    \let\@nextref\@next@ref% push processing to \@next@ref
  \fi%
  \@nextref#2%
}
\def\@next@ref#1,#2{%
   \ifx#2@ and~\ref{#1}\let\@nextref\@gobble% at end: print and+\ref and stop
   \else, \ref{#1}% print  ,+\ref and continue
   \fi%
   \@nextref#2%
}

\makeatother
\linespread{1.05} % Change line spacing here, Palatino benefits from a slight increase by default
\algnewcommand\Or{\textbf{or }}
\algnewcommand\And{\textbf{and}}
\makeatletter
\renewcommand\@biblabel[1]{\textbf{#1.}} % Change the square brackets for each bibliography item from '[1]' to '1.'
\renewcommand{\@listI}{\itemsep=0pt} % Reduce the space between items in the itemize and enumerate environments and the bibliography

\renewcommand{\maketitle}{ % Customize the title - do not edit title and author name here, see the TITLE block below
\begin{flushright} % Right align
{\LARGE\@title} % Increase the font size of the title

\vspace{50pt} % Some vertical space between the title and author name

{\large\@author} % Author name
\\\@date % Date

\vspace{40pt} % Some vertical space between the author block and abstract
\end{flushright}
}

%----------------------------------------------------------------------------------------
%	TITLE
%----------------------------------------------------------------------------------------

\title{\textbf{The Ising model}\\ % Title
      \small{\sc{Fast computation using the Metropolis Monte Carlo algorithm}}} % Subtitle

\author{\textsc{Maarten L. Terpstra -- s2028980} % Author
\\{\textit{Computing Science -- Computational Science \& Visualization}}
\\{\url{m.l.terpstra@student.rug.nl}}} % Institution

\date{\today} % Date

%----------------------------------------------------------------------------------------

\begin{document}
\maketitle % Print the title section
\usefont{T1}{cmr}{m}{n} % Computer Modern Roman (TeX default) in T1 encoding. May lead to bad text quality if you do not have cm-super installed.
%----------------------------------------------------------------------------------------
%	ABSTRACT AND KEYWORDS
%----------------------------------------------------------------------------------------

%\renewcommand{\abstractname}{Summary} % Uncomment to change the name of the abstract to something else

\begin{abstract}
In this report, the theoretical foundation and the practical results of the Ising model are discussed.
The main issues of gathering statistical information about an Ising model are discussed and a practical estimation of these statistics is provided in the form of the Metropolis-Monte Carlo algorithm. \end{abstract}

% \vspace{30pt} % Some vertical space between the abstract and first section

%----------------------------------------------------------------------------------------
%	ESSAY BODY
%----------------------------------------------------------------------------------------

\include{content/introduction}
\section{Results}
I have implemented the above algorithm for the one-dimensional and two-dimensional case. 
This implementation is written in Python in conjunction with the NumPy library. 
Plots were generated using Matplotlib. 
The source code for the one- and two-dimensional cases can be found in \autoref{sec:appA} and \autoref{sec:appB}, respectively.

The following tables (\autoref{tab:101000} through \autoref{tab:100010000}) give the results for the one dimensional case.
Above each table it denoted of how many atoms the one-dimensional lattice consisted (by \(N\)) and how many MMC steps were taken (by the number of samples).
The first column of each table denotes the temperature and the second column the inverse of the temperature, \(\beta\).
The third and fourth columns show the energy and specific heat per spin as found by the MMC method.
The fifth and sixth columns show the theoretical, optimal values for the lattice at that temperature regarding energy and specific heat. 
The final column shows the accuracy of the MMC method. 
This is computed as the average of the accuracy of the findings of the energy and the accuracy of the specific heat.

The first thing to note from all tables is that for all temperatures larger than 0.5 the found value is fairly close to the theoretical value. This is reflected in the accuracy percentage which is for almost all measurements larger than \(85\%\).
When the temperature is too low, the Boltzmann factor \(e^{-\beta\delta_E}\) is also very low so very few spin flips are accepted in the MMC algorithm. When the temperature increases, the accuracy also increases.

When the number of samples increase, the accuracy of the MMC also increases. 
This is to be expected as the it has a higher probability of ending up in a better configuration as more trials are performed. 
More noteworthy, the number of atoms is a direct influence on the accuracy: more atoms means a higher accuracy.
This is because the theoretical values only hold for an infinite system.
More atoms means a closer approximation of this infinite system, and can therefore result in a higher accuracy.
It is then no surprise that the best results among the trials is one with 1000 atoms and 10000 MMC steps.
This system achieves almost 100\% accuracy when compared to the theoretical values.

For sufficiently low temperatures, the system exhibits magnetic properties even in absence of an external magnetic field. 
That is, neighboring spins interact on each other in such a way that they produce a magnetic moment. This is true for the one-dimensional Ising model. It is then interesting to see for which temperature the system undergoes a phase transition from an ordered, spontaneously magnetized system to a disordered system that has no magnetic properties -- unless activated by an external magnetic field.
However, Ising has proven that the one-dimensional system does not undergo a phase transition\cite{ising1925beitrag} and we see no such transition in our results.
\newgeometry{top=1cm,left=3cm}
\include{content/1d_results}
\include{content/2d_results}
\restoregeometry
\section*{Conclusion}
The MMC method succeeds in finding (near)-optimal configurations of the Ising model with respect to the total energy of the system.
The results obtained are consistent with the mathematical results provided by Ising and Onsager, as there were no phase transitions found in the one-dimensional case.
However, there was a phase transition observed from the ordered, spontaneously magnetized case to the unordered situation without spontaneous magnetization.
This happens near the theoretical critical temperature \(T_c = \frac{2}{\ln(1 + \sqrt{2})} \approx 2.269\).
\appendix
\section{1D code}
\label{sec:appA}
\inputminted{python}{../Code/Python/MMC.py}
\section{2D code}
\label{sec:appB}
\inputminted{python}{../Code/Python/MMC_2D.py}
%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\bibliographystyle{unsrt}

\bibliography{sample}

%----------------------------------------------------------------------------------------

\end{document}