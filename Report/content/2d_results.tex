%!TEX root = ../report.tex
\subsection{2D results}
The two-dimensional case differs on several points from the one-dimensional case.
First, elements are now placed on a two-dimensional lattice instead of on a line.
This means that particles can not only interact with their left and right neighbor, but also with those above and below it.
This changes the definition of the energy of the system to:
\[E = -\sum_{i = 1}^{N-1}\sum_{j=1}^{N}S_{i,j}S_{i+1,j} - \sum_{i = 1}^{N}\sum_{j=1}^{N-1}S_{i,j}S_{i,j+1}.\]

Contrary to the one-dimensional case, a phase transition \emph{is} possible in the two-dimensional case as proven by Onsager \cite{onsager1944crystal}, which was also attempted to observe here.
To do this, there are again statistics collected during the MMC computation to determine the average energy per spin \(U / N^2\), specific heat per spin \(C / N^2\) and the average magnetization per spin \(\langle M\rangle / N^2\).
The magnetization of a configuration is determined by \(M = \sum_i \sigma_i\). 
The average magnetization is thus \(\langle M\rangle = \sum_{m\in M} m / \vert M\vert\).

In \Autoref{fig:stats1000, fig:stats10000} show each 6 scatter plots. 
The first row of each figure shows the average energy per spin the system exhibits for a certain temperature. 
The second row of each figure shows the specific heat per spin for a certain temperature.
The third row of each figure shows the average magnetization per spin for each temperature.
The left column of each figure shows the plots for 20 different temperatures whereas the right column shows the same plot for 200 different temperatures.

As we can see from the plots, a phase transition is occurring here.
This is visible in all the scatter plots. 
In the energy plots (\Autoref{fig:e100020,fig:e1000200,fig:e1000020,fig:e10000200}), we can see that the energy begins to increase starting from a temperature of about 1.5. 
This increase reaches its apex between \(2.2 \leq t \leq 2.5\) after which the energy increase begins to slow down.
This is also confirmed in the specific heat plots (\Autoref{fig:c100020,fig:c1000200,fig:c1000020,fig:c10000200}).
As the specific heat is the derivative of the energy w.r.t. temperature, we can interpret the peak at a temperature of about 2.3 as the temperature at which the speed of energy increase reaches a maximum. This is thus also where the phase transition happens.
Not coincidentally, the critical temperature \(T_c = \frac{2}{\ln(1 + \sqrt{2})} \approx 2.269\).
For the systems with \(10\times10\) atoms, this peak is further away from \(T_c\) because this system is too small and the equations describing the infinite system do not reasonably describe the small, finite system.

In the magnetization plots (\Autoref{fig:m100020,fig:m1000200,fig:m1000020,fig:m10000200}) we can also see a clear phase transition. 
For temperatures below about 2.3, there is very often a non-zero average magnetization.
This means that spontaneous magnetization is occurring. 
However, beyond the critical temperature, spontaneous magnetization is not anymore occuring, because order disappears in the system due to a too high temperature. There are too many spins flipping, causing spontaneous magnetization to disappear.
\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.36]{content/images/energies-1000-20.eps}
\caption{}
\label{fig:e100020}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.36]{content/images/energies-1000-200.eps}
\caption{}
\label{fig:e1000200}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.36]{content/images/heats-1000-20.eps}
\caption{}
\label{fig:c100020}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.36]{content/images/heats-1000-200.eps}
\caption{}
\label{fig:c1000200}
\end{subfigure}
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.36]{content/images/magnetizations-1000-20.eps}
\caption{}
\label{fig:m100020}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.36]{content/images/magnetizations-1000-200.eps}
\caption{}
\label{fig:m1000200}
\end{subfigure}
\caption{Scatter plots for 1000 MMC steps. Figures \subref{fig:e100020},\subref{fig:c100020} and \subref{fig:m100020} show the average energy, specific heat and average magnetization for 20 different temperatures, respectively. Figures \subref{fig:e1000200},\subref{fig:c1000200} and \subref{fig:m1000200} show the same for 200 different temperatures.}
\label{fig:stats1000}
\end{figure}
\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.35]{content/images/energies-10000-20.eps}
\caption{}
\label{fig:e1000020}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.35]{content/images/energies-10000-200.eps}
\caption{}
\label{fig:e10000200}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.35]{content/images/heats-10000-20.eps}
\caption{}
\label{fig:c1000020}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.35]{content/images/heats-10000-200.eps}
\caption{}
\label{fig:c10000200}
\end{subfigure}
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[scale=.35]{content/images/magnetizations-10000-20.eps}
\caption{}
\label{fig:m1000020}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\keytip{Please appreciate this image. It took 4 days to generate on 8 cores}\includegraphics[scale=.35]{content/images/magnetizations-10000-200.eps}
\caption{}
\label{fig:m10000200}
\end{subfigure}
\caption{Scatter plots for 10000 MMC steps. Figures \subref{fig:e100020},\subref{fig:c100020} and \subref{fig:m100020} show the average energy, specific heat and average magnetization for 20 different temperatures, respectively. Figures \subref{fig:e1000200},\subref{fig:c1000200} and \subref{fig:m1000200} show the same for 200 different temperatures.}
\label{fig:stats10000}
\end{figure}