%!TEX root = ../report.tex
\section{Introduction}
A world as we live in today does not exist without magnets.
Besides that Earth itself is one huge magnets guarding us from space radiation and debris, magnets are used in a plethora of applications, ranging from navigation, food preparation, storing data, producing sound, driving trains and detecting diseases.
While we know how to interact and manipulate the macroscopic properties of magnets, the underlying mathematical theory can help uncover more interesting non-trivial properties, which may allow us to derive even more applications of magnets.
An example of such non-trivial property is that of an phase transition. 
A phase transition is what happens when a system goes to one phase to another -- e.g. water that freezes at a certain temperature.
What can happen with magnets during a phase transition is that spontaneous magnetization occurs, or stops occurring.
A strong mathematic model can help us analyze a problem and achieve a deeper understanding of macroscopic and microscopic properties.
One such model, the Ising model, arose from statistical mechanics and gives a mathematical basis for (ferro)magnetism.
\subsection{The Ising Model}
The Ising model a long-studied, simple lattice model to describe the macroscopic magnetic properties of a thermodynamic system.
Recently \cite{cuevas2014simple}, it was found to be an universal spin model, that even allows mapping of arbitrary problems to Ising models.
This could help us to gain more insight in very hard, or NP-Complete, problems.
It consists of a number of atoms with spins that can have a value of \(\sigma_i = \pm 1\).
These spins can interact with each other, modifying each others spins and changing the energy of the system.
The energy is calculated according to the Hamiltonian
\[
	\mathcal{H} = -J\underset{i,j}{\sum}\sigma_i\sigma_j - H\underset{i}{\sum}\sigma_i
\]
which gives us the total energy of the system. 
Here, \(J\) is the interaction constant, \(H\) is the magnetic moment -- i.e. how a spin interacts with the external field -- and \(i\) and \(j\) are neighbors.
\(E\) refers to the energy of a specific configuration. The energy of a particular one-dimensional configuration \(S\) is given by \[E = -\sum_{n=1}^{N-1}S_nS_{n+1}.\] Note that this implement so called ``free-edges'' boundary conditions: elements at the boundary have no neighbor beyond the boundary.
When \(J > 0\), this means that the material is ferromagnetic. 
This means that the system ``favors'' configurations where all the spins point in the same direction -- i.e. all configurations where \(i\) and \(j\) are neighbors, \(\sigma_i = \sigma_j\) have a higher probability of occurring.
When \(J < 0\), the material is anti-ferromagnetic -- i.e. it favors configurations where neighboring spins are different than the spin of a particle.

For the remainder we use that \(J = 1\) and \(H = 0\) -- i.e. a ferromagnetic material without an external field.
The Ising model can be used to study how the system behaves as a function of the (thermodynamic) temperature \(T\).
Statistical mechanics tells us that the probability that a configuration \(c\) is realized with energy \(E\) follows from the probability distribution
\[
	P_c = \frac{e^{-\beta E_c}}{\underset{\{S_0, S_1, \ldots, S_n\}}{\sum}e^{-\beta E}}
\]
where \(\beta = \frac{1}{T}\). The denominator -- which is also called the partition function, often denoted by \(Z\) --  sums over all possible configurations, i.e. \(Z = e^{-\beta E_1} + e^{-\beta E_2} + \ldots\). The free energy of a system is thus the weighted average of all possible configurations \( U = \sum_c E_c P_c = -\frac{\partial}{\partial\beta}\ln Z\).
Since we know that \(Z = (2\cosh \beta) ^ N\) we can express \( U\) in terms of \(\beta\) by 
\begin{align*}
     U  &= -\frac{\partial}{\partial\beta}\ln Z  \\ 
    				  &= -\frac{\partial}{\partial\beta}\ln \left( 2^N \cosh^N\beta\right)\\
    				  &= -\frac{\partial\ln u}{\partial u}\frac{\partial u}{\partial \beta}\,\left(\!\text{with } u = 2^N\cosh^N\beta\text{ and } \frac{\partial\ln u}{\partial u} = \frac{1}{u}\right)\\
    				  &= -\frac{\frac{\partial}{\partial\beta}\left(2^N\cosh^N\beta\right)}{2^N\cosh^N\beta}= -\frac{2^N\frac{\partial}{\partial\beta}\left(\cosh^N\beta\right)}{2^N\cosh^N\beta}\\
    				  &= -\frac{\frac{\partial}{\partial\beta}\left(\cosh^N\beta\right)}{\cosh^N\beta}\\
    				  &= -\frac{N\cosh^{N-1}(\beta)\frac{\partial}{\partial\beta}\cosh\beta}{\cosh^N\beta}\\
    				  &= -\frac{N\frac{\partial}{\partial\beta}\cosh\beta}{\cosh\beta}\\
    				  &= -N\frac{\sinh\beta}{\cosh\beta} = -N\tanh\beta.
\end{align*}
Therefore, the average energy per spin \(\frac{U}{N} = -\tanh\beta\).
In the same way, we can derive an exact solution for the specific heat.
In short, the specific heat \(C\) gives the fluctuation of the energy as a function of the temperature. 
By this description, we can determine that
\begin{align*}
	C &= \frac{\partial U}{\partial T} 
		= -\frac{\beta}{T}\frac{\partial U}{\partial\beta}
		= -\beta^2\frac{\partial U}{\partial\beta}.
		% &= \beta^2 \frac{\partial^2\ln Z}{\partial\beta^2}\\
		% &= \beta^2 \frac{\partial}{\partial\beta}\left(\frac{1}{Z}\frac{\partial Z}{\partial\beta}\right) \\
		% &= \beta^2 \left\lbrack\frac{1}{Z} \frac{\partial^2 Z}{\partial\beta^2} - \frac{1}{Z^2}\left(\frac{\partial Z}{\partial\beta}\right)^2\right\rbrack\\
		% &= \beta^2 \left(\frac{\underset{\{S_1,\ldots, S_n\}}{\sum} E^2 e^{-\beta E}}{\underset{\{S_1,\ldots, S_n\}}{\sum} e^{-\beta E}} - U^2\right)
\end{align*}
Therefore, \[C / N = -\frac{\beta^2}{N}\frac{\partial U}{\partial\beta} = \left(\frac{\beta}{\cosh\beta}\right)^2.\]

Now that it is known which macroscopic results are possible at a given temperature, it is not known \emph{which} configurations achieve these results. 
One possibility is to try every possible configuration and evaluate \(P_c\).
Since every atom out of \(N\) atoms on the lattice can obtain two different spins, there are \(2^N\) possible configurations.
Computing this for a sufficiently large square lattice this would require an infeasible amount of configurations -- i.e. a \(20 \times 20\) lattice would evaluating \(2^{400}\) configurations.
Assuming one can compute \(10^{12}\) sums per second -- which is optimistic --  this would still take several thousands of years.
One way to circumvent this, is by using the Metropolis-Monte Carlo Algorithm.

\subsection{Metropolis-Monte Carlo}
Metropolis-Monte Carlo (hereafter, MMC) is an algorithm to generate configurations drawn from an unknown distribution.
It obtains a sequence of random samples from a random distribution.
With regard to the Ising model, it is a very simple algorithm:

\begin{algorithmic}
	\Procedure{MMC}{$n$}
		\State \(S\gets \text{Random configuration}\)
		\State \(E\gets E(S)\) \Comment{i.e. \(E\) is the energy of the configuration}
		\While{\(i < n\)}
			\State \(j \gets random()\cdot n + 1\) \Comment{\(random() \in [0, 1)\)}
			\State \(\delta_E \gets \text{change in energy if } S(j) = -S(j)\)
			\If {\(e^{-\beta\delta_E} > random()\)}
				\Comment{Accept the spin flip}
				\State \(S(j) \gets -S(j)\)
				\State \(E \gets E + \delta_E\)
			\EndIf
			\State \(i \gets i + 1\)
		\EndWhile
		\State \Return E
	\EndProcedure
\end{algorithmic}

A new state is generated based on the previous state, similar to a Markov process.
This algorithm implements a technique which is known as importance sampling.
It attempts to find the states which are most important, i.e. have a configuration as close to optimal as possible.
In order to do so it performs \(n\) samples where any atom might have its spin flipped in order to see whether this improves the configuration.
If it does -- meaning the total energy is lowered -- the flip is accepted, otherwise it is accepted with a probability proportionate to the energy change.
This is done to avoid local minima. 
Because the more important configurations must be found first, one typically starts recording statistics regarding the energy, specific heat and magnetization after some waiting period. 
This allows the system to ``relax'' such that measurements are not "noised" by unimportant configurations.
Typically, the number of waiting steps is \(\frac{n}{10}\).
